import React, {Component} from 'react';
import './Header.css';
import logo from './HeaderLogo.svg';

class Header extends Component {
    render() {
        return (
            <div className="header">
                <img src={logo} className="header-logo" alt="logo"/>
                <h2>Welcome to React</h2>
            </div>
        );
    }
}

export default Header;