import React, { Component } from 'react';
import './App.css';
import Header from '../Header';
import MainContainer from '../MainContainer';
import Footer from '../Footer';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Header/>
                <MainContainer/>
                <Footer/>
            </div>
        );
    }
}

export default App;
